import sys
from time import sleep

import pygame
import settings
from game_stats import GameStats
from scoreboard import Scoreboard
from ship import Ship
from bullet import Bullet
from alien import Alien
from button import Button

class AlienInvasion:
    """overall class to manage game assets and behaviour"""

    def __init__(self):
        """Initialize the game, create resources"""
        pygame.init()
        self.settings = settings.Settings()

        #self.screen = pygame.display.set_mode((self.settings.screen_width,
        #                                       self.settings.screen_height))
        self.screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
        self.settings.screen_width = self.screen.get_rect().width
        self.settings.screen_height = self.screen.get_rect().height
        pygame.display.set_caption("Alien Invasion")
        #create an instance to store game statistics
        # and create a scoreboard
        self.stats = GameStats(self)
        self.sb = Scoreboard(self)
        self.ship = Ship(self)
        self.bullets = pygame.sprite.Group()
        self.aliens = pygame.sprite.Group()

        self._create_fleet()

        #make the play button
        self.play_button = Button(self, "Play")

    def run_game(self):
        """Start the main loop"""
        while True:
            self._check_events()

            if self.stats.game_active:
                self.ship.update()
                self._update_bullets()
                self._update_aliens()

            self._update_screen()

    def _check_events(self):
        """watch for keyboard and mouse events""" 
        for event in pygame.event.get():    
            if event.type == pygame.QUIT:        
                sys.exit()                       
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)                           
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                self._check_play_button(mouse_pos)


    def _check_keydown_events(self, event):
        """Respond to key presses"""
        if event.key == pygame.K_RIGHT:
            #move ship to right
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            #move ship to left
            self.ship.moving_left = True
        elif event.key == pygame.K_SPACE:
            #shoot bullet
            self._fire_bullet()
        elif event.key == pygame.K_q:
            sys.exit()

    def _check_keyup_events(self, event):
        """Respond to key releases"""
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False 
        if event.key == pygame.K_LEFT:        
            self.ship.moving_left = False     

    def _fire_bullet(self):
        """create new bullet and add it to bullets group"""
        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet(self)
            self.bullets.add(new_bullet)

    def _create_fleet(self):
        """Create the fleet of aliens"""

        #Create an alien and find number of aliens in a row
        #spacing between each alien is equal to one alien width
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        available_width = self.settings.screen_width - ( 2 * alien_width)
        num_aliens_per_row = available_width // (2 * alien_width)

        #determine the number of rows of aliens that fit on the screen
        ship_height = self.ship.rect.height
        available_height = (self.settings.screen_height - 
                            (3 * alien_height) - ship_height)
        number_rows = available_height // (2 * alien_height)

        #create full fleet of aliens
        for row_number in range(0, number_rows):
            #create first row of aliens
            for alien_number in range(0, num_aliens_per_row):
                #create an alien and place it in a row
                self._create_alien(alien_number, row_number)

    def _create_alien(self, alien_number, row_number):
        """Create an alien and place it in the row"""
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        alien.x = alien_width + (2 * alien_width * alien_number)
        alien.rect.x = alien.x
        alien.rect.y = alien_height + (2 * alien_height * row_number)
        self.aliens.add(alien)

    def _update_aliens(self):
        """check if the fleet is at an edge
            then update the position of the alien fleet"""
        self._check_fleet_edges()
        self.aliens.update()
        #look for alien-ship collisons
        if pygame.sprite.spritecollideany(self.ship, self.aliens):
           self._ship_hit()

        #look for aliens hitting the bottom of the screen
        self._check_aliens_bottom()

    def _ship_hit(self):
        """Respond to the ship being hit by an alien"""

        if self.stats.ships_left > 0:
            # Decrement ships left, and update scoreboard
            self.stats.ships_left -= 1
            self.sb.prep_ships()

            # Get rid of remaining ships and bullets
            self.bullets.empty()
            self.aliens.empty()

            # Create ne wfleet and center the ship
            self._create_fleet()
            self.ship.center_ship()

            #pause
            sleep(1)
        else:
            self.stats.game_active = False
            pygame.mouse.set_visible(True)


    def _check_aliens_bottom(self):
        """Check if any aliens have hit the bottom of the screen"""
        screen_rect = self.screen.get_rect()
        for alien in self.aliens.sprites():
            if alien.rect.bottom >= screen_rect.bottom:
                self._ship_hit()
                break



    def _check_fleet_edges(self):
        for alien in self.aliens.sprites():
            if alien.check_edges():
                self._change_fleet_direction()
                break

    def _change_fleet_direction(self):
        """Drop the entire fleet and change fleet's direction"""
        for alien in self.aliens.sprites():
            alien.rect.y += self.settings.fleet_drop_speed
        self.settings.fleet_direction *= -1

    def _update_screen(self):
        """Update images on screen and flip to the new screen"""
        self.screen.fill(self.settings.bg_color)
        self.ship.update() 
        self.ship.blitme()
        for bullet in self.bullets.sprites():
            bullet.draw_bullet()
        self.aliens.draw(self.screen)

        #draw the score information
        self.sb.show_score()

        #draw the play button if the game is inactive
        if not self.stats.game_active:
            self.play_button.draw_button()

        pygame.display.flip()

    def _update_bullets(self):
        """Update position of existing bullets and remove off-screen bullets"""
        #update bullet positions
        self.bullets.update()
        # Check for any bullets that have hit aliens
        #  if so, get rid of that bullet and alien
        self._check_bullet_alien_collisons()

    def _check_bullet_alien_collisons(self):
        collisons = pygame.sprite.groupcollide(
                    self.bullets, self.aliens, True, True)
        
        if collisons:
            for aliens in collisons.values():
                self.stats.score += self.settings.alien_points * len(aliens)
            self.sb.prep_score()
            self.sb.check_high_score()

        if not self.aliens:
            #destroy remaining bullets and create new fleet
            self.bullets.empty()
            self._create_fleet()
            self.settings.increase_speed()

            #increase level
            self.stats.level += 1
            self.sb.prep_level()

        #Remove off-screen bullets
        for bullet in self.bullets.copy():
            if bullet.rect.bottom < 0:
                self.bullets.remove(bullet)

    def _check_play_button(self, mouse_pos): 
        """Start a new game when the player clicks play"""
        button_clicked = self.play_button.rect.collidepoint(mouse_pos)
        if button_clicked and not self.stats.game_active :
            """reset the game stats"""
            #reset the game settings
            self.settings.initialize_dynamic_settings()
            self.stats.reset_stats()
            self.stats.game_active = True
            self.sb.prep_score()
            self.sb.prep_level()
            self.sb.prep_ships()
            #get rid of any remaining aliens and bullets
            self.aliens.empty()
            self.bullets.empty()

            #create a new fleet and center the ship
            self._create_fleet()
            self.ship.center_ship()
            
            #hide the mouse cursor
            pygame.mouse.set_visible(False)

if __name__ == "__main__":
    ai = AlienInvasion()
    ai.run_game()


