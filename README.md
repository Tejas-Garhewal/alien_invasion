In _Alien Invasion_ the player  control a ship that is located at the bottom 
centre of the screen. The player can _move_ their ship using the _left_ and
_right_ arrow keys and shoot using _spacebar_. When the game begins, a fleet of
aliens fill the sky, ie, the topmost region of the screen, and moves across
and down the screen. The player shoots and destroys the aliens. If the 
player shoots the entire fleet, a new fleet appears that is faster than 
the previous one. If any alien _hits the ship_ or _reaches the bottom of the
screen_, the player _loses a ship_. If the _player loses three ships_, they _lose_.

